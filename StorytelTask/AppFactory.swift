import Foundation
import Dip

final class AppFactory {
    
    private let container = DependencyContainer()
    var window: UIWindow { (try? container.resolve()) ?? UIWindow() }
    
    func registerDependencies() {
        
        //Register app window
        container.register(.weakSingleton) { () -> UIWindow in
            
            let window = UIWindow()
            try window.rootViewController = self.container.resolve() as BooksViewController
            window.makeKeyAndVisible()
            return window
        }
        
        //Register books module dependencies
        BooksFactory().registerDependencies(container: container)
    }
}

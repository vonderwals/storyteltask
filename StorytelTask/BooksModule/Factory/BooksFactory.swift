import Foundation
import Dip

final class BooksFactory {
    
    func registerDependencies(container: DependencyContainer) {
        
        container.register {
            BooksServiceImpl() as BooksService
        }
        container.register {
            ThrottlingServiceImpl() as ThrottlingService
        }
        
        //Register BooksView
        container.register(.weakSingleton) {
            BooksViewController()
        }
        .implements(BooksView.self)
        .resolvingProperties { container, view in
            view.presenterHolder = try container.resolve() as BooksModule
        }
        
        //Register BooksModule
        container.register(.weakSingleton) {
            BooksPresenter(booksService: try container.resolve(), throttlingService: try container.resolve())
        }
        .implements(BooksModule.self)
        .resolvingProperties { container, presenter in
            presenter.view = try container.resolve() as BooksView
            //Set default query
            presenter.query = "harry"
        }
    }
}

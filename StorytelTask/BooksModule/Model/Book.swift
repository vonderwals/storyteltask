import Foundation

final class Book: Decodable {
    
    let title: String
    let imageUrl: URL
    
    private let authors: [Participant]
    private let narrators: [Participant]
        
    private enum CodingKeys: String, CodingKey {
        case title
        case cover
        case authors
        case narrators
    }
    
    private enum CoverCoddingKeys: String, CodingKey {
        case url
    }
    
    init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        title = try container.decode(String.self, forKey: .title)
        authors = try container.decode([Participant].self, forKey: .authors)
        narrators = try container.decode([Participant].self, forKey: .narrators)
        
        let coverContainer = try container.nestedContainer(keyedBy: CoverCoddingKeys.self, forKey: .cover)
        imageUrl = try coverContainer.decode(URL.self, forKey: .url)
    }
    
    init(title: String, imageUrl: URL, authors: [Participant], narrators: [Participant]) {
        
        self.title = title
        self.imageUrl = imageUrl
        self.authors = authors
        self.narrators = narrators
    }
}

extension Book {
    
    var authorsTitle: String {
        makeTitle(from: authors, label: "By")
    }
    
    var narratorTitle: String {
        makeTitle(from: narrators, label: "With")
    }
    
    private func makeTitle(from participants: [Participant], label: String) -> String {
        guard participants.count > 0 else { return "" }
        
        var title = participants.reduce("\(label):") { "\($0) \($1.name)," }
        
        //Remove ,
        title.removeLast()
        return title
    }
}

final class Participant: Decodable {
    
    let name: String
    
    private enum CodingKeys: String, CodingKey {
        case name
    }
    
    init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decode(String.self, forKey: .name)
    }
    
    init(name: String) {
        self.name = name
    }
}

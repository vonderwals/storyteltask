import Foundation

enum BookDisplayItem {
    case header(String)
    case book(Book)
}

extension BookDisplayItem: TableElement { }

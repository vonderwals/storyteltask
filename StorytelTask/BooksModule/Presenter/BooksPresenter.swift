import Foundation

final class BooksPresenter: BooksModule {    
    
    private let booksService: BooksService
    private let throttlingService: ThrottlingService
    private var nextPageToken: String?
    private var isLoading = false
    private var items: [BookDisplayItem] = [] {
        didSet {
            view?.reloadTable()
        }
    }
    
    init(booksService: BooksService, throttlingService: ThrottlingService) {
        
        self.booksService = booksService
        self.throttlingService = throttlingService
    }
        
    weak var view: BooksView? {
        didSet {
            setupView()
        }
    }
    
    //MARK: - BooksModule
    
    var query: String?
    
    //MARK: - Private
    
    private func setupView() {
        
        view?.onViewDidLoad = { [weak self] in
            
            self?.view?.setSearchText(searchText: self?.query ?? "")
            self?.loadBooks()
        }
        
        view?.tableRow = { [weak self] in
            return self?.items[$0]
        }
        
        view?.tableRowsCount = { [weak self] in
            return self?.items.count ?? 0
        }
        
        view?.onNextPage = { [weak self] in
            self?.loadMore()
        }
        
        view?.onSearch = { [weak self] searchText in
            guard searchText != "" else {
                self?.clearItems()
                return
            }
            
            self?.nextPageToken = nil
            self?.query = searchText
            self?.loadBooks()
        }
    }
    
    private func loadBooks() {
        
        throttlingService.throttle { [weak self] in
            
            self?.booksService.fetchBooks(query: self?.query ?? "", nextPageToken: self?.nextPageToken) { [weak self] result in
                switch result {
                case .success(let response):
                    self?.items = [.header("Query: \(response.query)")] + response.books.map { BookDisplayItem.book($0) }
                    self?.nextPageToken = response.nextPageToken
                    self?.query = response.query
                case .failure(let error):
                    self?.view?.showError(error: error)
                }
            }
        }
    }
    
    private func loadMore() {
        guard !isLoading else { return }
        
        isLoading = true
        view?.startLoadingAnimation()
        booksService.fetchBooks(query: query ?? "", nextPageToken: nextPageToken) { [weak self] result in
            
            self?.view?.stopLoadingAnimation()
            self?.isLoading = false
            
            switch result {
            case .success(let response):
                self?.items += response.books.map { BookDisplayItem.book($0) }
                self?.nextPageToken = response.nextPageToken
                self?.query = response.query
            case .failure(let error):
                self?.view?.showError(error: error)
            }
        }
    }
    
    private func clearItems() {
        
        items = []
        nextPageToken = nil
        query = nil
    }
}

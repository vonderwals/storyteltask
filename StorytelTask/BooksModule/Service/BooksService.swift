import Foundation

protocol BooksService: class {
    func fetchBooks(query: String, nextPageToken: String?, completion: @escaping (Result<BooksResponse, Error>)->())
}

final class BooksServiceImpl: BooksService {
    
    private let baseUrlString = "https://api.storytel.net/search"
    
    func fetchBooks(query: String, nextPageToken: String?, completion: @escaping (Result<BooksResponse, Error>) -> ()) {
        guard let url = makeSearchUrl(query: query, page: nextPageToken) else { return }

        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                DispatchQueue.main.async {
                    completion(.failure(ApplicationError.networkError))
                }
                return
            }
            guard let result = try? JSONDecoder().decode(BooksResponse.self, from: data) else {
                DispatchQueue.main.async {
                    completion(.failure(ApplicationError.parseError))
                }
                return
            }
             
            DispatchQueue.main.async {
                completion(.success(result))
            }
        }.resume()
    }
    
    private func makeSearchUrl(query: String?, page: String?) -> URL? {
        
        var urlComponents = URLComponents(string: baseUrlString)
        urlComponents?.queryItems = []
        if let query = query {
            urlComponents?.queryItems?.append(URLQueryItem(name: "query", value: query))
        }
        if let page = page {
            urlComponents?.queryItems?.append(URLQueryItem(name: "page", value: page))
        }
        return urlComponents?.url
    }
}

class BooksResponse: Decodable {
    
    let books: [Book]
    let nextPageToken: String
    let query: String
    
    private enum CodingKeys: String, CodingKey {
        case items
        case nextPageToken
        case query
    }
    
    required init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        nextPageToken = try container.decode(String.self, forKey: .nextPageToken)
        books = try container.decode([Book].self, forKey: .items)
        query = try container.decode(String.self, forKey: .query)
    }
    
    init(books: [Book], nextPageToken: String, query: String) {
        
        self.books = books
        self.nextPageToken = nextPageToken
        self.query = query
    }
}

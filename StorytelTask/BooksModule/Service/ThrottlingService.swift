import Foundation

protocol ThrottlingService: class {
    func throttle(task: @escaping () -> ())
}

final class ThrottlingServiceImpl: ThrottlingService {
    
    private var currentWorkItem: DispatchWorkItem?
    private let throttleQueue = DispatchQueue(label: "throttleQueue")
    
    func throttle(task: @escaping () -> ()) {
        
        currentWorkItem?.cancel()
        
        let workItem = DispatchWorkItem(block: task)
        currentWorkItem = workItem
        
        throttleQueue.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: workItem)
    }
}

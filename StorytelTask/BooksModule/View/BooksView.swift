import Foundation

protocol BooksView: ViewLifecycleObserver, ErrorDisplayable, TableDataSource {
    
    var onNextPage: (() -> ())? { get set }
    var onSearch: ((String) -> ())? { get set }
    
    func startLoadingAnimation()
    func stopLoadingAnimation()
    func setSearchText(searchText: String)
}

import UIKit

class BooksViewController: BaseViewController, BooksView {
    
    private let bookCellName = String(describing: BookCell.self)
    private let headerCellName = String(describing: QueryHeaderCell.self)
    
    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        
        let activityIndicatorView = UIActivityIndicatorView(style: .medium)
        activityIndicatorView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 44)
        return activityIndicatorView
    }()
    
    private lazy var tableView: UITableView = {
        
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(BookCell.self, forCellReuseIdentifier: bookCellName)
        tableView.register(QueryHeaderCell.self, forCellReuseIdentifier: headerCellName)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        tableView.allowsSelection = false
        return tableView
    }()
    
    private lazy var searchBar: UISearchBar = {
        
        let searchBar = UISearchBar()
        searchBar.delegate = self
        return searchBar
    }()
    
    //MARK: - Books View
    
    var tableRowsCount: (() -> Int)?
    var tableRow: ((Int) -> TableElement?)?
    var rowWasSelected: ((Int) -> ())?
    var onNextPage: (() -> ())?
    var onSearch: ((String) -> ())?
    
    func startLoadingAnimation() {
        
        activityIndicatorView.startAnimating()
        tableView.tableFooterView = activityIndicatorView
    }
    
    func stopLoadingAnimation() {
        
        activityIndicatorView.stopAnimating()
        tableView.tableFooterView = nil
    }
    
    func reloadTable() {
        tableView.reloadData()
    }
    
    func setSearchText(searchText: String) {
        searchBar.text = searchText
    }
    
    //MARK: - View Controller Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        setupSubviews()
    }
    
    //MARK: - Private
    
    private func setupSubviews() {
        
        view.addSubview(searchBar)
        view.addSubview(tableView)
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            searchBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            searchBar.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
            searchBar.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
            
            tableView.topAnchor.constraint(equalTo: searchBar.bottomAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            tableView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor)
        ])
    }
}

extension BooksViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableRowsCount?() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let displayItem = tableRow?(indexPath.row) as? BookDisplayItem else { return UITableViewCell() }
        
        switch displayItem {
        case .header(let headerTitle):
            guard let headerCell = tableView.dequeueReusableCell(withIdentifier: headerCellName, for: indexPath) as? QueryHeaderCell else {
                return UITableViewCell()
            }
            
            headerCell.setup(with: headerTitle)
            return headerCell
        case .book(let book):
            guard let bookCell = tableView.dequeueReusableCell(withIdentifier: bookCellName, for: indexPath) as? BookCell else {
                return UITableViewCell()
            }
            
            bookCell.setup(with: book)
            return bookCell
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard searchBar.text?.isEmpty == false && searchBar.text != nil else { return }

        if ((tableView.contentOffset.y + tableView.frame.size.height) >= tableView.contentSize.height) {
            onNextPage?()
        }
    }
}

extension BooksViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        onSearch?(searchText)
    }
}


import Foundation
import UIKit
import Kingfisher

final class BookCell: UITableViewCell {
    
    private lazy var titleLabel = UILabel()
    private lazy var authorsLabel = UILabel()
    private lazy var narratorsLabel = UILabel()
    
    private lazy var bookImageView = UIImageView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(with book: Book) {
        
        titleLabel.text = book.title
        authorsLabel.text = book.authorsTitle
        narratorsLabel.text = book.narratorTitle
        bookImageView.kf.setImage(with: book.imageUrl)
    }
    
    private func setupSubviews() {
        
        addSubview(bookImageView)
        addSubview(titleLabel)
        addSubview(authorsLabel)
        addSubview(narratorsLabel)
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        narratorsLabel.translatesAutoresizingMaskIntoConstraints = false
        authorsLabel.translatesAutoresizingMaskIntoConstraints = false
        bookImageView.translatesAutoresizingMaskIntoConstraints = false
        
        let bookImageHeightConstraint = bookImageView.heightAnchor.constraint(equalToConstant: 100)
        bookImageHeightConstraint.priority = UILayoutPriority(rawValue: 999)
        NSLayoutConstraint.activate([
            bookImageView.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            bookImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
            bookImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 8),
            bookImageHeightConstraint,
            bookImageView.widthAnchor.constraint(equalTo: bookImageView.heightAnchor),
            
            titleLabel.leftAnchor.constraint(equalTo: bookImageView.rightAnchor, constant: 10),
            titleLabel.topAnchor.constraint(equalTo: bookImageView.topAnchor),
            titleLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -8),
            
            authorsLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 8),
            authorsLabel.leftAnchor.constraint(equalTo: titleLabel.leftAnchor),
            authorsLabel.rightAnchor.constraint(equalTo: titleLabel.rightAnchor),
            
            narratorsLabel.topAnchor.constraint(equalTo: authorsLabel.bottomAnchor, constant: 8),
            narratorsLabel.leftAnchor.constraint(equalTo: titleLabel.leftAnchor),
            narratorsLabel.rightAnchor.constraint(equalTo: titleLabel.rightAnchor)
        ])
    }
}

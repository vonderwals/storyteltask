import Foundation

enum ApplicationError: Error {
    case networkError
    case parseError
    case unknownError
}

import UIKit

/**
    Defines that your module can handle errors
 */
@objc protocol ErrorDisplayable: class {
    func showError(title: String, description: String)
    func showError(error: Error)
}

protocol ErrorHandleable: class {
    func handleError(error: Error) -> String
}

extension BaseViewController: ErrorDisplayable {
    
    func showError(title: String = "Error", description: String = "Something wrong") {
        
        let alertController = UIAlertController(title: title, message: description, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alertController, animated: true)
    }
    
    func showError(error: Error) {
        if let error = error as? ApplicationError {
            switch error {
            case .networkError:
                showError(description: "Network error")
            case .parseError:
                showError(description: "System error")
            case .unknownError:
                showError()
            }
        } else {
            showError()
        }
    }
}

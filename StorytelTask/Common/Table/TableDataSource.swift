protocol TableDataSource: class {
    func reloadTable()
    var tableRowsCount: (() -> Int)? { get set }
    var tableRow: ((_ row: Int) -> TableElement?)? { get set }
    var rowWasSelected: ((_ row: Int) -> ())? { get set }
}

protocol TableElement { }

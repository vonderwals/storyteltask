import UIKit

class BaseViewController: UIViewController, ViewLifecycleObserver {
    
    var presenterHolder: AnyObject?
    
    // MARK: - Lifecycle
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        onViewDidLoad?()
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        onViewWillAppear?()
    }
    
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        onViewDidAppear?()
    }
    
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        onViewWillDisappear?()
    }
    
    override open func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        onViewDidDisappear?()
    }
    
    // MARK: - ViewLifecycleObserver
    public var onViewDidLoad: (() -> ())? {
        didSet {
            if isViewLoaded {
                onViewDidLoad?()
            }
        }
    }
    public var onViewWillAppear: (() -> ())?
    public var onViewDidAppear: (() -> ())?
    public var onViewWillDisappear: (() -> ())?
    public var onViewDidDisappear: (() -> ())?
    
}

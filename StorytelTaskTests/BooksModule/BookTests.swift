import XCTest
@testable import StorytelTask

class BookTests: XCTestCase {
    
    func testEmpty() {
        //Given
        let book = Book(title: "test", imageUrl: URL(string: "https://www.test.com")!, authors: [], narrators: [])
        
        //When
        let authorsTitle = book.authorsTitle
        let narratorsTitle = book.narratorTitle
        
        //Then
        XCTAssertEqual(authorsTitle, "")
        XCTAssertEqual(narratorsTitle, "")
    }
    
    func testOneParticipant() {
        
        //Given
        let book = Book(title: "test", imageUrl: URL(string: "https://www.test.com")!, authors: [Participant(name: "test")], narrators: [Participant(name: "test")])
        
        //When
        let authorsTitle = book.authorsTitle
        let narratorsTitle = book.narratorTitle
        
        //Then
        XCTAssertEqual(authorsTitle, "By: test")
        XCTAssertEqual(narratorsTitle, "With: test")
    }
    
    func testMultipleParticipant() {
        //Given
        let book = Book(title: "test", imageUrl: URL(string: "https://www.test.com")!, authors: [Participant(name: "test1"), Participant(name: "test2")], narrators: [Participant(name: "test1"), Participant(name: "test2")])
        
        //When
        let authorsTitle = book.authorsTitle
        let narratorsTitle = book.narratorTitle
        
        //Then
        XCTAssertEqual(authorsTitle, "By: test1, test2")
        XCTAssertEqual(narratorsTitle, "With: test1, test2")
    }
}

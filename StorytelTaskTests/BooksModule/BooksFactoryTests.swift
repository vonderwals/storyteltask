import XCTest
import Dip
@testable import StorytelTask

class BooksFactoryTests: XCTestCase {
    
    var container: DependencyContainer!

    override func setUpWithError() throws {
        container = DependencyContainer()
    }

    override func tearDownWithError() throws {
        container.reset()
        container = nil
    }

    func testDependencies() throws {
        let factory = BooksFactory()
        factory.registerDependencies(container: container)
        
        let view = try container.resolve() as BooksView
        let module = try container.resolve() as BooksModule
        let service = try container.resolve() as BooksService
        
        XCTAssertNotNil(view)
        XCTAssertNotNil(module)
        XCTAssertNotNil(service)
    }
}

import Foundation
@testable import StorytelTask

final class BooksViewMock: BaseViewController, BooksView {
    
    var setSearchTextWasCalled = false
    func setSearchText(searchText: String) {
        setSearchTextWasCalled = true
    }
    
    var startAnmationWasCalled = false
    func startLoadingAnimation() {
        startAnmationWasCalled = true
    }
    
    var stopAnimationWasCalled = false
    func stopLoadingAnimation() {
        stopAnimationWasCalled = true
    }
    
    var showErrorWasCalled = false
    override func showError(title: String, description: String) {
        showErrorWasCalled = true
    }
    
    var reloadTableWasCalled = false
    func reloadTable() {
        reloadTableWasCalled = true
    }
    
    var tableRowsCount: (() -> Int)?
    var tableRow: ((Int) -> TableElement?)?
    var rowWasSelected: ((Int) -> ())?
    var onNextPage: (() -> ())?
    var onSearch: ((String) -> ())?
}

final class BooksServiceMock: BooksService {
    
    func fetchBooks(query: String, nextPageToken: String?, completion: @escaping (Result<BooksResponse, Error>) -> ()) {
        let book = Book(title: "testBook", imageUrl: URL(string: "https://www.test.com")!, authors: [Participant(name: "author")], narrators: [Participant(name: "narrator")])
        let response = BooksResponse(books: [book], nextPageToken: "test", query: "test")
        completion(.success(response))
    }
}

final class BooksServiceErrorMock: BooksService {
    
    func fetchBooks(query: String, nextPageToken: String?, completion: @escaping (Result<BooksResponse, Error>) -> ()) {
        completion(.failure(ApplicationError.unknownError))
    }
}

final class ThrottlingServiceMock: ThrottlingService {
    func throttle(task: @escaping () -> ()) {
        task()
    }
}

import XCTest
@testable import StorytelTask

class BooksModuleTests: XCTestCase {
    
    func testTableDataSource() {
        
        //Given
        let (view, _) = makeMockedBooksModule(service: BooksServiceMock())
        
        //When
        view.onViewDidLoad?()
        let count = view.tableRowsCount?()
        let firstElement = view.tableRow?(0) as? BookDisplayItem
        let secondElement = view.tableRow?(1) as? BookDisplayItem
        
        //Then
        XCTAssertEqual(2, count)
        
        if case .header = firstElement {
            XCTAssert(true)
        } else {
            XCTFail()
        }
        
        if case .book = secondElement {
            XCTAssert(true)
        } else {
            XCTFail()
        }
    }
    
    func testFetchData() {
        
        //Given
        let (view, _) = makeMockedBooksModule(service: BooksServiceMock())
        
        //When
        view.onViewDidLoad?()
        
        //Then
        XCTAssertTrue(view.reloadTableWasCalled)
        XCTAssertTrue(view.setSearchTextWasCalled)
        XCTAssertFalse(view.startAnmationWasCalled)
        XCTAssertFalse(view.stopAnimationWasCalled)
        XCTAssertFalse(view.showErrorWasCalled)
    }
    
    func testNextPage() {
        
        //Given
        let (view, _) = makeMockedBooksModule(service: BooksServiceMock())
        
        //When
        view.onNextPage?()
        
        //Then
        XCTAssertTrue(view.startAnmationWasCalled)
        XCTAssertTrue(view.stopAnimationWasCalled)
        XCTAssertTrue(view.reloadTableWasCalled)
        XCTAssertFalse(view.showErrorWasCalled)
        XCTAssertFalse(view.setSearchTextWasCalled)
    }
    
    func testError() {
        
        //Given
        let (view, _) = makeMockedBooksModule(service: BooksServiceErrorMock())
        
        //When
        view.onViewDidLoad?()
        
        //Then
        XCTAssertTrue(view.showErrorWasCalled)
        XCTAssertTrue(view.setSearchTextWasCalled)
        XCTAssertFalse(view.reloadTableWasCalled)
        XCTAssertFalse(view.startAnmationWasCalled)
        XCTAssertFalse(view.stopAnimationWasCalled)
    }
    
    private func makeMockedBooksModule(service: BooksService) -> (BooksViewMock, BooksModule) {
        let presenter = BooksPresenter(booksService: service, throttlingService: ThrottlingServiceMock())
        let view = BooksViewMock()
        presenter.view = view
        view.presenterHolder = presenter
        
        return (view, presenter)
    }
}
